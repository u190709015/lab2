public class FindMin {

    public static void main(String[] args){
        int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        int result;
        int result1;

        boolean someCondition = value1 < value2 ;
        result = someCondition ? value1 : value2;

        boolean anotherboolean = result < value3;
        result1 = anotherboolean ? result : value3;

        System.out.println(result1);

    }
}